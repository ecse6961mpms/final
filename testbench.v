`timescale 1ns / 1ps

module testbench;
    
    // Inputs
	reg clk;
	reg [31:0] mem_din;
	reg [31:0] mem_instr_din;

	// Outputs
	wire [31:0] mem_dout;
	wire [31:0] mem_addr;
	wire [31:0] mem_instr_addr;
	wire mem_write;
	wire mem_read;
	wire mem_instr_read;

	belt_cpu dut (
		.clk(clk), 
		.mem_din(mem_din),
		.mem_instr_din(mem_instr_din), 
		.mem_dout(mem_dout), 
		.mem_addr(mem_addr), 
		.mem_instr_addr(mem_instr_addr),
		.mem_write(mem_write), 
		.mem_read(mem_read),
		.mem_instr_read(mem_instr_read)
	);

	reg [31:0] memory[0:1023];

	initial begin
		$readmemh("mem.list", memory);
	end

    always @(negedge clk) begin
        if(mem_addr < 32'h100) begin
            if(mem_write) memory[mem_addr/4] <= mem_dout;
		    else if (mem_read) mem_din <= memory[mem_addr/4];
        end else if (mem_addr == 32'h804) begin
            if (mem_read) mem_din <= {24'd0, 8'haa};
        end
	end

	always @(negedge clk) begin
		if(mem_instr_addr < 32'h100) begin
            if (mem_instr_read) mem_instr_din <= memory[mem_instr_addr/4]; 
        end
	end

    always #10 clk = ~clk;

    initial begin
        $dumpfile("testbench.vcd");
        $dumpvars(0,dut);
        clk = 0;
        mem_din = 0;
		mem_instr_din = 0;

        #5000;

        $finish;
    end

endmodule
