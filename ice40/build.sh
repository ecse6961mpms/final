rm mem.list soc.json soc.asc soc.bin
python3 ../assemble.py prog.asm mem.list
yosys -p "synth_ice40 -dsp -json soc.json" soc.v
nextpnr-ice40 --up5k --package sg48 --json soc.json --pcf soc.pcf --asc soc.asc
icepack soc.asc soc.bin