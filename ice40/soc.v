`include "../belt_cpu.v"

module soc_top(
    input clk_12mhz,
    output adclk,
    input [7:0] addata,
    output daclk,
    output [7:0] dadata
    );

    wire clk;

    reg [3:0] counter;

    initial begin
        counter = 4'd0;
    end

    always @(posedge clk_12mhz) begin
        // if(counter == 272) counter <= 0;
        counter <= counter + 1;
    end

    assign clk = counter[3];
    // assign LED = ~counter[25:18];


    reg [31:0] mem_din;
	reg [31:0] mem_instr_din;
	wire [31:0] mem_dout;
	wire [31:0] mem_addr;
	wire [31:0] mem_instr_addr;
	wire mem_write;
	wire mem_read;
	wire mem_instr_read;

    reg [31:0] memory[0:30];

	initial begin
		$readmemh("mem.list", memory);
	end

    always @(negedge clk) begin
        if(mem_addr < 32'h400) begin
            if(mem_write) memory[mem_addr/4] <= mem_dout;
		    else if (mem_read) mem_din <= memory[mem_addr/4];
        end else if (mem_addr == 32'h800) begin
            if (mem_write) dadata <= (mem_dout[16:0])+128;
        end else if (mem_addr == 32'h804) begin
            if (mem_read) mem_din <= {8'd0, addata[7:0]}-128;
        end
	end

    assign daclk = mem_write;
    assign adclk = mem_read;

	always @(negedge clk) begin
		if(mem_instr_addr < 32'h400) begin
            if (mem_instr_read) mem_instr_din <= memory[mem_instr_addr/4]; 
        end
	end

    belt_cpu belt_cpu(
        .clk(clk),
        .mem_din(mem_din),
        .mem_instr_din(mem_instr_din),
        .mem_dout(mem_dout),
        .mem_addr(mem_addr),
        .mem_instr_addr(mem_instr_addr),
        .mem_write(mem_write),
        .mem_read(mem_read),
        .mem_instr_read(mem_instr_read)
    );

endmodule
