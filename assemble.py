import sys

def num(s):
    if "h" in s:
        return int(s[0:-1], base=16)
    n = int(s)
    if n < 0:
        n = 0xFFFF + n + 1
    return n

def is_instr(ins):
    instrs = ["add", "addi", "sub", "subi", "mul", "muli", "load", "store", "jump"]
    for valid in instrs:
        if ins == valid: return True
    return False

def load(line):
    opcode = 0b000001
    imm = num(line[1])
    return f"{(opcode<<26 | imm):08X}"

def store(line):
    opcode = 0b000010
    rs1 = int(line[1])
    imm = num(line[2])
    return f"{(opcode<<26 | rs1<<21 | imm):08X}"

def jump(line):
    opcode = 0b000011
    imm = num(line[1])
    return f"{(opcode<<26 | imm):08X}"

def alu(line):
    opcodes = {"add":0b100100, "addi":0b101100, "sub":0b010100, "subi":0b010100, "mul":0b110100, "muli":0b111100}
    opcode = opcodes[line[0]]
    if line[0][-1] == "i":
        rs1 = 0
        rs2 = int(line[1])
        imm = num(line[2])
    else:
        rs1 = int(line[1])
        rs2 = int(line[2])
        imm = 0
    return f"{(opcode<<26 | rs1<<21 | rs2<<16 | imm):08X}"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"usage: {sys.argv[0]} infile outfile")
        sys.exit(1)

    infilename = sys.argv[1]
    outfilename = sys.argv[2]

    infile = open(infilename, "r")

    mem = ["00000000"]*1024
    addr = 0

    for line in infile.readlines():
        line = line.strip().split()
        if len(line) == 0: continue

        if line[0] == ".org":
            addr = int(num(line[1])/4)
            continue

        if is_instr(line[0]):
            if line[0] == "load":
                mem[addr] = load(line)
            elif line[0] == "store":
                mem[addr] = store(line)
            elif line[0] == "jump":
                mem[addr] = jump(line)
            else:
                mem[addr] = alu(line)
        else:
            mem[addr] = f"{num(line[0]):08X}"

        addr += 1

    outfile = open(outfilename, "w")
    for line in mem:
        outfile.write(line)
        outfile.write("\n")