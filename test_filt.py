

if __name__ == "__main__":
    samples = [0xAA] * 10

    a0 = 0.005
    a1 = 0.01
    a2 = 0.005
    b1 = -1.799
    b2 = 0.817

    n=8
    a0 = int(a0 * 2**8)
    a1 = int(a1 * 2**8)
    a2 = int(a2 * 2**8)
    b1 = int(b1 * 2**8)
    b2 = int(b2 * 2**8)

    print(f"a0: {a0}, a1: {a1}, a2: {a2}")
    print(f"b1: {b1}, b2: {b2}")

    z1_in = 0
    z2_in = 0
    z1_out = 0
    z2_out = 0

    samples_out = []
    for i in range(len(samples)):
        in_s = float(samples[i])

        m1 = (int(a0*in_s)>>n)
        m2 = (int(a1*z1_in)>>n)
        m3 = (int(a2*z2_in)>>n)
        m4 = (int(b1*z1_out)>>n)
        m5 = (int(b2*z2_out)>>n)
        print(f"in: {in_s}, z1_in: {z1_in}, z2_in: {z2_in}, z2_out: {z2_out}, z1_out: {z1_out}")
        print(f"m1: {m1}, m2: {m2}, m3: {m3}, m4: {m4}, m5: {m5}")
        s1 = m1 + m2
        s2 = s1 + m3
        s3 = s2 - m4
        s4 = s3 - m5
        print(f"s1: {s1}, s2: {s2}, s3: {s3}, s4: {s4}")

        out_s = (int(a0*in_s)>>n) + (int(a1*z1_in)>>n) + (int(a2*z2_in)>>n) - (int(b1*z1_out)>>n) - (int(b2*z2_out)>>n)
        print(out_s)
        z2_in = z1_in
        z1_in = in_s
        z2_out = z1_out
        z1_out = out_s
        samples_out.append(int(out_s))
        print("")


    print(samples)
    print(samples_out)