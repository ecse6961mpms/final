module belt_cpu(
    input clk,
    input [31:0] mem_din,
    input [31:0] mem_instr_din,
    output [31:0] mem_dout,
    output [31:0] mem_addr,
    output [31:0] mem_instr_addr,
    output mem_write,
    output mem_read,
    output mem_instr_read
    );

    localparam ALU   = 6'bXXX100;
    localparam ADD   = 6'b100100;
    localparam ADDI  = 6'b101100;
    localparam SUB   = 6'b010100;
    localparam SUBI  = 6'b011100;
    localparam MUL   = 6'b110100;
    localparam MULI  = 6'b111100;

    localparam LOAD  = 6'b000001;
    localparam STORE = 6'b000010;
    localparam JMP   = 6'b000011;
    localparam NOP   = 6'b000000;


    reg [31:0] pc;
    reg [31:0] instr_fetch;
    reg [31:0] instr_execute;
    reg [31:0] instr_writeback;

    initial begin
        pc <= 32'hFFFFFFFC;
        instr_fetch <= 32'h0;
        instr_execute <= 32'h0;
        instr_writeback <= 32'h0;
    end

    //datapath connections
    wire [15:0] alu_A;
    wire [15:0] alu_out;
    reg [15:0] alu_out_mem;
    reg [15:0] alu_out_writeback;
    wire [15:0] belt_din;
    wire [15:0] r1_out;
    reg [15:0] r1_out_mem;
    wire [15:0] r2_out;
    wire [31:0] new_pc;
    reg [15:0] mem_din_writeback;
    wire [15:0] r1;
    wire [15:0] r2;
    wire [4:0] rs1;
    wire [4:0] rs2;

    //control signals
    wire push;

    //muxes
    assign r1 = (instr_execute[25:21] == 5'd0) && push ? belt_din : r1_out;
    assign r2 = (instr_execute[20:16] == 5'd0) && push ? belt_din : r2_out;
    assign rs1 = push ? instr_execute[25:21] - 1 : instr_execute[25:21];
    assign rs2 = push ? instr_execute[20:16] - 1 : instr_execute[20:16];
    assign alu_A = instr_execute[29] ? instr_execute[15:0] : r1;
    assign belt_din = instr_writeback[28] ? alu_out_mem : mem_din_writeback;
    assign new_pc = (instr_fetch[31:26] == JMP) ? {16'b0, instr_fetch[15:0]} : pc+4;

    assign mem_addr = {16'b0, instr_execute[15:0]};
    assign mem_dout = push ? {16'b0, alu_out_mem} : {16'b0, r1_out};
    assign mem_instr_addr = new_pc;//(instr_fetch[31:26] == JMP) ? new_pc : pc;

    assign mem_write = (instr_execute[31:26] == STORE);
    assign mem_read = (instr_execute[31:26] == LOAD);
    assign mem_instr_read = 1'b1;
    assign push = (instr_writeback[28]) | (instr_writeback[31:26] == LOAD);

    belt belt(
        .clk(~clk),
        .rs1(rs1),
        .rs2(rs2), 
        .din(belt_din),
        .push(push),
        .r1_out(r1_out),
        .r2_out(r2_out)
    );

    alu alu(
        .clk(~clk),
        .A(alu_A),
        .B(r2),
        .func(instr_execute[31:30]),
        .out(alu_out)
    );

    always @(posedge clk) begin
        instr_fetch <= mem_instr_din;
        instr_execute <= instr_fetch;
        instr_writeback <= instr_execute;

        //FETCH
        pc <= new_pc;
        // mem_instr_read <= 1;

        //EXECUTE and MEM
        r1_out_mem <= r1_out;
        alu_out_mem <= alu_out;

        //WRITEBACK
        alu_out_writeback <= alu_out_mem;
        mem_din_writeback <= mem_din;
       

    end

endmodule

module belt(
    input clk,
    input [4:0] rs1,
    input [4:0] rs2,
    input [15:0] din,
    input push,
    output [15:0] r1_out,
    output [15:0] r2_out
    );

    reg [15:0] belt[0:31];
    reg [4:0] belt_mask;

    integer i;
    initial begin
        belt_mask <= 5'd0;
         for(i = 0; i < 32; i = i + 1) begin
             belt[i] <= 15'h0;
         end
    end 

    always @(negedge clk) begin
        if(push) begin
            belt[belt_mask] <= din;
        end
    end

    always @(posedge clk) begin
        if(push) begin
            belt_mask <= belt_mask + 1;
        end        
    end

    wire [4:0] r1_i;
    wire [4:0] r2_i;
    assign r1_i = belt_mask - rs1;
    assign r2_i = belt_mask - rs2;

    assign r1_out = belt[r1_i];
    assign r2_out = belt[r2_i];

endmodule

module alu(
    input clk,
    input [15:0] A,
    input [15:0] B,
    input [1:0] func,
    output reg [15:0] out
    );

    initial begin
        out <= 16'b0;
    end

    always @(posedge clk) begin
        case (func)
            2'b10: out <= A + B;
            2'b01: out <= A - B;
            2'b11: out <= ({{8{A[15]}}, A} * {{8{B[15]}}, B})>>8;
        endcase
    end

endmodule

