# ECSE 6961 Final Project

Implementation and demonstration of Mill computing's Belt register system.

Desciption
---
Mill computing is attempting to create a new general purpose architecture family called the Mill. One of the innovations in this architecture is called "the belt" where instead of using overwritable registers, data is added to a stack-like structure. Data on the belt is immutable, and will be destroyed if the belt exceeds the length provided. For example, if the belt is 16 long, the data will "fall off" once it reaches postition 16. In this stack, the elements are effectively referenced by time with element #0 being the newest result added, #1 being the previous, to #15 being the oldest still on the belt in a 16-long belt. Results from instructions are presumed added to the belt, which has the side effect of instructions and functions able to return multiple results.

By making the data immutable data hazards are effectively eliminated, decreasing the hardware required to mitigate data hazards.

The goal of this project is to implement a Belt system in a processor and create a demonstration of the belt operating.

Contents of this Repository
---

The CPU with the belt register system is implemented in `belt_cpu.v`. A testbench for the CPU is found in `testbench.v`. To simulate the CPU, use `sim.sh` which requires iVerilog. Generated VCD files can be viewed with GTKWave. An implementation of the CPU for synthesis on an FPGA is in the folder ice40, and uses the yosys+nextpnr toolchain. 